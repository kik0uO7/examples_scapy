#!/bin/python
from scapy.all import *
 
# Target host
host = '192.168.1.41'

# Interface name
iface = 'wlp1s0'

def get_packets(pkt):
    if pkt[IP].src == host:
        data = pkt.load[-4:].decode()
        print(f"{data}", flush=True, end='')

sniff(prn=get_packets, filter='icmp', iface=iface)

